/*!

=========================================================
* Black Dashboard React v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/black-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/black-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Dashboard from "views/Dashboard.js";
import Drivers from "views/Drivers.js";
import Karts from "./views/Karts";

var routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "tim-icons icon-chart-pie-36",
    component: Dashboard,
    layout: "/admin",
  },
  {
    path: "/drivers",
    name: "Drivers",
    icon: "tim-icons icon-single-02",
    component: Drivers,
    layout: "/admin",
  },
  {
    path: "/karts",
    name: "Karts",
    icon: "tim-icons icon-delivery-fast",
    component: Karts,
    layout: "/admin",
  },
];
export default routes;
