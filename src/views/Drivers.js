import React from "react";

import { Card, CardHeader, CardBody, Row, Col } from "reactstrap";
import {useAllDrivers} from "../hooks/use_all_drivers";

const Drivers = () => {

  const {drivers} = useAllDrivers();

  return (
    <>
      <div className="content">
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <h5 className="title">{drivers.length} Personnages Mario Kart</h5>
                <p className="category">
                  Inspiré du jeu officiel{" "}
                  <a href="https://nucleoapp.com/?ref=1712">Mario Kart 8 Deluxe</a>
                </p>
              </CardHeader>
              <CardBody className="all-icons">
                <Row>
                  {drivers.map(driver => (
                      <Col
                          key={driver.name}
                          className="font-icon-list col-xs-6 col-xs-6"
                          lg="2"
                          md="3"
                          sm="4"
                      >
                        <div className="font-icon-detail">
                          <i className="tim-icons icon-single-02" />
                          <p>{driver?.name}</p>
                        </div>
                      </Col>
                  ))}

                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    </>
  );
}

export default Drivers;
